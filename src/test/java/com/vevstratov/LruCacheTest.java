package com.vevstratov;

import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class LruCacheTest {

    @Test
    public void testGetFromEmptyCache() throws Exception {
        // given
        Cache<Integer, Integer> cache = new LruCache<>(1);

        // when

        // then
        assertNull(cache.get(1));
    }

    @Test
    public void testGet() throws Exception {
        // given
        Cache<Integer, Integer> cache = new LruCache<>(1);

        // when
        cache.put(1, 1);

        // then
        assertNotNull(cache.get(1));
    }

    @Test
    public void testOneElementCacheNotGrowing() throws Exception {
        // given
        Cache<Integer, Integer> cache = new LruCache<>(1);

        // when
        cache.put(1, 1);
        cache.put(2, 2);

        // then
        Map map = (Map) Whitebox.getInternalState(cache, "map");
        assertTrue(map.size() == 1);
    }

    @Test
    public void testOneElementCacheUsesLru() throws Exception {
        // given
        Cache<Integer, Integer> cache = new LruCache<>(1);

        // when
        cache.put(1, 1);
        cache.put(2, 2);

        // then
        assertNull(cache.get(1));
        assertTrue(cache.get(2) == 2);
    }

    @Test
    public void testCacheUsesLru() throws Exception {
        // given
        Cache<Integer, Integer> cache = new LruCache<>(3);

        // when
        Map<Integer, Integer> map = new HashMap<>();
        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
        cache.get(2);
        cache.put(4, 4);
        cache.put(5, 5);
        map.put(2, 2);
        map.put(4, 4);
        map.put(5, 5);

        // then
        Map cacheInnerMap = (Map) Whitebox.getInternalState(cache, "map");
        assertEquals(map, cacheInnerMap);
    }

}