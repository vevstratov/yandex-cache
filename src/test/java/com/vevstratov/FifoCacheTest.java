package com.vevstratov;

import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class FifoCacheTest {

    @Test
    public void testGetFromEmptyCache() throws Exception {
        // given
        Cache<Integer, Integer> cache = new FifoCache<>(1);

        // when

        // then
        assertNull(cache.get(1));
    }

    @Test
    public void testGet() throws Exception {
        // given
        Cache<Integer, Integer> cache = new FifoCache<>(1);

        // when
        cache.put(1, 1);

        // then
        assertNotNull(cache.get(1));
    }

    @Test
    public void testOneElementCacheNotGrowing() throws Exception {
        // given
        Cache<Integer, Integer> cache = new FifoCache<>(1);

        // when
        cache.put(1, 1);
        cache.put(2, 2);

        // then
        Map map = (Map) Whitebox.getInternalState(cache, "map");
        assertTrue(map.size() == 1);
    }

    @Test
    public void testOneElementCacheUsesFifo() throws Exception {
        // given
        Cache<Integer, Integer> cache = new FifoCache<>(1);

        // when
        cache.put(1, 1);
        cache.put(2, 2);

        // then
        assertNull(cache.get(1));
        assertTrue(cache.get(2) == 2);
    }

    @Test
    public void testCacheUsesFifo() throws Exception {
        // given
        int capacity = 5;
        Cache<Integer, Integer> cache = new FifoCache<>(capacity);

        // when
        int i = 0;
        for (; i < capacity; ++i) {
            cache.put(i, i);
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (; i < capacity * 2; ++i) {
            map.put(i, i);
            cache.put(i, i);
        }

        // then
        Map cacheInnerMap = (Map) Whitebox.getInternalState(cache, "map");
        assertEquals(map, cacheInnerMap);
    }

}