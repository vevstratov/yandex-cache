package com.vevstratov;

import java.util.LinkedHashMap;

/**
 * Created by Viktor Evstratov
 * on 11.09.2014
 */
public class LruCache<K, V> extends AbstractCache<K, V> {
    public LruCache(int capacity) {
        super(capacity, new LinkedHashMap<K, V>(capacity + 1, 1, true));
    }

    @Override
    protected void releaseElement() {
        map.remove(getLeastRecentKey());
    }

    private K getLeastRecentKey() {
        return map.keySet().iterator().next();
    }
}
