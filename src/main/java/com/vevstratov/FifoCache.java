package com.vevstratov;

import java.util.LinkedHashMap;

/**
 * Created by Viktor Evstratov
 * on 11.09.2014
 */
public class FifoCache<K, V> extends AbstractCache<K, V> {
    public FifoCache(int capacity) {
        super(capacity, new LinkedHashMap<K, V>(capacity + 1, 1));
    }

    @Override
    protected void releaseElement() {
        map.remove(getFirst());
    }

    private K getFirst() {
        return map.keySet().iterator().next();
    }
}
