package com.vevstratov;

/**
 * Created by Viktor Evstratov
 * on 11.09.2014
 */
public interface Cache<K, V> {
    V put(K key, V value);

    V get(K key);
}
