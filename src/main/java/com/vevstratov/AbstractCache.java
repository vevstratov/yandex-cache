package com.vevstratov;

import java.util.Map;

/**
 * Created by Viktor Evstratov
 * on 11.09.2014
 */
public abstract class AbstractCache<K, V> implements Cache<K, V> {
    private final int capacity;
    protected final Map<K, V> map;

    protected AbstractCache(int capacity, Map<K, V> map) {
        this.capacity = capacity;
        this.map = map;
    }

    @Override
    public V put(K key, V value) {
        if (map.size() == capacity) {
            releaseElement();
        }
        return map.put(key, value);
    }

    @Override
    public V get(K key) {
        return map.get(key);
    }

    protected abstract void releaseElement();
}
